import echarts from '../lib/echarts.min';
import {Component} from "rainbowui-desktop-core";
import "../css/component.css";
import "../lib/theme";
import PropTypes from 'prop-types';

export default class Pie extends Component {

    renderComponent() {
        return (
            <div className="uiChars">
                <div className={this.props.styleClass} style={this.props.style} id={this.componentId} >

                </div>
            </div>
        );
    }

    componentDidMount(){
        const chartObject = this.initOption();
        this.initEvent(chartObject);
        window.addEventListener('resize', this.Resize.bind(this));
    }

    componentDidUpdate(){
        const chartObject = this.initOption();
        chartObject.resize(); 
        window.addEventListener('resize', this.Resize.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.Resize.bind(this))
    }

    Resize() {
        const chartObject = this.initOption();
        setTimeout(function (){
            window.onresize = function () {
                if (chartObject) {
                    chartObject.resize();
                }
            }
        },200)
    }
    initOption(){
        const myChart = document.getElementById(this.componentId);
        if (myChart) {
            const chartObject = echarts.init(myChart,this.props.theme, {renderer: this.props.renderer});
            chartObject.setOption(this.props.option);          
            return chartObject;
        }
    }

    initEvent(chartObject) {
        let _self = this;
        chartObject.on('click', function (params) {
            if (_self.props.onClick) {
                _self.props.onClick(params);
            }
        });
        chartObject.on('dblclick', function (params) {
            if (_self.props.onDoubleClick) {
                _self.props.onDoubleClick(params);
            }
        });
        chartObject.on('pieselectchanged', function (params) {
            if (_self.props.onPieSelectChanged) {
                _self.props.onPieSelectChanged(params);
            }
        });
        chartObject.on('legendscroll', function (params) {
            if (_self.props.onLegendscroll) {
                _self.props.onLegendscroll(params);
            }
        });
    }


};

Pie.propTypes = $.extend({}, Component.propTypes, {
    option:PropTypes.object,
    theme:PropTypes.string,
    onClick: PropTypes.func,
    renderer: PropTypes.string,
    onDoubleClick: PropTypes.func,
    onPieSelectChanged: PropTypes.func,
    onLegendscroll: PropTypes.func
});


Pie.defaultProps = $.extend({}, Component.defaultProps, {
    theme:"default",
    renderer: "canvas",
    style:{"width":"100%","height":"400px"},
    option: {
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b}: {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        x: 'left',
        data:['直接访问','邮件营销','联盟广告','视频广告','搜索引擎']
    },
    series: [
        {
            name:'访问来源',
            type:'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                normal: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    show: true,
                    textStyle: {
                        fontSize: '30',
                        fontWeight: 'bold'
                    }
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data:[
                {value:335, name:'直接访问'},
                {value:310, name:'邮件营销'},
                {value:234, name:'联盟广告'},
                {value:135, name:'视频广告'},
                {value:1548, name:'搜索引擎'}
            ]
        }
    ]
}
});



